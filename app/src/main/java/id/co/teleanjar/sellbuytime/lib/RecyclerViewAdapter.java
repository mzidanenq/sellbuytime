package id.co.teleanjar.sellbuytime.lib;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.util.ArrayList;
import java.util.logging.Handler;

import de.hdodenhof.circleimageview.CircleImageView;
import id.co.teleanjar.sellbuytime.BuyTimeProductOrder1Activity;
import id.co.teleanjar.sellbuytime.BuyTimeProductOrderReviewActivity;
import id.co.teleanjar.sellbuytime.ChatDetailActivity;
import id.co.teleanjar.sellbuytime.ProgressBuyerOnGoingWorkStatusActivity;
import id.co.teleanjar.sellbuytime.ProgressSellerOrderBiddingActivity;
import id.co.teleanjar.sellbuytime.R;
import id.co.teleanjar.sellbuytime.RegSellerActivity;
import id.co.teleanjar.sellbuytime.RegSellerExperienceAddActivity;
import id.co.teleanjar.sellbuytime.RegSellerExperienceFragment;
import id.co.teleanjar.sellbuytime.RegSellerProductAddActivity;
import id.co.teleanjar.sellbuytime.ReviewOrderRemarkActivity;
import id.co.teleanjar.sellbuytime.SellTimeActivity;
import id.co.teleanjar.sellbuytime.SellTimeBiddingActivity;
import id.co.teleanjar.sellbuytime.SellTimePremium;
import id.co.teleanjar.sellbuytime.SettingBankAccountActivity;
import id.co.teleanjar.sellbuytime.SettingEditBankAccountActivity;
import id.co.teleanjar.sellbuytime.model.ChatModel;
import id.co.teleanjar.sellbuytime.model.OnGoingModel;

import static android.support.constraint.Constraints.TAG;
import static android.support.v4.content.ContextCompat.getColor;
import static android.support.v4.content.ContextCompat.startActivity;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    ArrayList<String> mImage;
    ArrayList<String> mData1;
    ArrayList<String> mData2;
    ArrayList<String> mData3;
    ArrayList<String> mData4;
    ArrayList<String> mData5;
    ArrayList<String> mData6;
    ArrayList<String> mratingBar;
    ArrayList<ChatModel> mChatModel = new ArrayList<>();
    ArrayList<OnGoingModel> mOnGoingModel = new ArrayList<>();
    Fragment fragment;
    String jenis;
    Context mContext;
    View view;
    Integer itemview;

    public RecyclerViewAdapter(String jenis, Context mContext, ArrayList<String> mImage, ArrayList<String> mData1,ArrayList<String> mData2, ArrayList<String> mData3, ArrayList<String> mData4, ArrayList<String> mData5, ArrayList<String> mData6, ArrayList<String> mratingBar) {
        this.jenis = jenis;
        this.mImage = mImage;
        this.mData1 = mData1;
        this.mData2 = mData2;
        this.mData3 = mData3;
        this.mData4 = mData4;
        this.mData5 = mData5;
        this.mData6 = mData6;
        this.mratingBar = mratingBar;
        this.mContext = mContext;
    }

    //with fragment and simple input
    public RecyclerViewAdapter(String jenis,Context mContext,Fragment fragment,ArrayList<String> mData1,ArrayList<String> mData2, ArrayList<String> mData3, ArrayList<String> mData4, ArrayList<String> mData5, ArrayList<String> mData6) {
        this.mData1 = mData1;
        this.mData2 = mData2;
        this.mData3 = mData3;
        this.mData4 = mData4;
        this.mData5 = mData5;
        this.mData6 = mData6;
        this.fragment = fragment;
        this.mContext = mContext;
        this.jenis = jenis;
    }

    public RecyclerViewAdapter(String jenis, Context mContext, ArrayList<ChatModel> mChatModel) {
        this.mContext = mContext;
        this.mChatModel = mChatModel;
        this.jenis = jenis;
    }

    public RecyclerViewAdapter(String jenis, ArrayList<OnGoingModel> mOnGoingModel, Context mContext) {
        this.mContext = mContext;
        this.mOnGoingModel = mOnGoingModel;
        this.jenis = jenis;
    }

    @Override
    public int getItemViewType(int position) {
        switch (jenis) {
            case "chatdetail":
            if (mChatModel.get(position).getUser().equals("pembeli")) {
                itemview = 1;
            } else {
                itemview = 0;
            }
            break;
            default:
                itemview = super.getItemViewType(position);
                break;
        }
        return itemview;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (jenis) {
            case "listitem":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_message, parent,false);
                break;
            case "listjeniskeahlian":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_profile_jeniskeahlian, parent,false);
                break;
            case "buytime":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_buytime, parent, false);
                break;
            case "review":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_review_postorder, parent, false);
                break;
            case "feedback":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_feedback, parent, false);
                break;
            case "bankaccount":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_bank_account, parent, false);
                break;
            case "experience":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_profile_experience, parent, false);
                break;
            case "selltime":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_selltime, parent, false);
                break;
            case "registerexperience":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_regseller_experience_add_expandable,parent, false);
                break;
            case "expproduct":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_regseller_product_add_expandable, parent, false);
                break;
            case "iklan":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_feedback, parent, false);
                break;
            case "productorder":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_buytime_product_order, parent, false);
                break;
            case "infotransfer":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_info_trf,parent, false);
                break;
            case "sellerreview":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_profile_review, parent,false);
                break;
            case "chatdetail":
                switch (viewType) {
                    case 1:
                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_detailmessage_sent, parent, false);
                        break;
                    case 0:
                        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_detailmessage_receive, parent, false);
                        break;
                }
                break;
            case "progressbuyerorder":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_review_postorder, parent,false);
                break;
            case "profilechatlist":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_message, parent, false);
                break;
            case "banklist":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_bank_list, parent, false);
                break;
            case "historyorder":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_history_order, parent, false);
                break;
            case "ongoingwork":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_ongoing_work, parent, false);
                break;
            case "orderoffering":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_history_order, parent, false);
                break;
            case "owforseller":
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_ongoing_work, parent, false);
                break;
        }

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        switch (jenis) {
            case "listitem":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image);
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Toast.makeText(mContext, mData1.get(position), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mContext,ChatDetailActivity.class);
                        mContext.startActivity(intent);
                    }
                });
                break;
            case "listjeniskeahlian":
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext, mData1.get(position), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case "buytime":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image);
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.data4.setText(mData4.get(position));
                holder.data5.setText(mData5.get(position));
                holder.data6.setText(mData6.get(position));
                holder.ratingBar.setRating(Float.valueOf(mratingBar.get(position)));
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, BuyTimeProductOrder1Activity.class);
                        mContext.startActivity(intent);
                        Toast.makeText(mContext, mData1.get(position), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case "review":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image);
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.data4.setText(mData4.get(position));
                holder.data5.setText(mData5.get(position));
                holder.data6.setText(mData6.get(position));
                holder.ratingBar.setRating(Float.valueOf(mratingBar.get(position)));
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, ReviewOrderRemarkActivity.class);
                        mContext.startActivity(intent);
                    }
                });
                break;
            case "feedback":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image1);
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("text/html");
                        share.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>This is text</p>"));
                        mContext.startActivity(Intent.createChooser(share, "Share Using"));
                    }
                });
                break;
            case "bankaccount":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image1);
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(view.getContext(), SettingEditBankAccountActivity.class);
                        mContext.startActivity(intent);
                    }
                });
                holder.btn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

                        builder.setMessage("Apakah anda yakin ingin menghapus akun "+holder.data2.getText().toString()+" ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        new android.os.Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                Snackbar snackbar = Snackbar
                                                        .make(view, "Bank Account is deleted", Snackbar.LENGTH_LONG)
                                                        .setAction("UNDO", new View.OnClickListener() {

                                                            @Override
                                                            public void onClick(View view) {
                                                                Snackbar snackbar1 = Snackbar.make(view, "Bank Account is restored!", Snackbar.LENGTH_SHORT);
                                                                snackbar1.show();
                                                            }
                                                        });

                                                snackbar.show();
                                            }
                                        }, 2000);
                                    }
                                })
                                .setNegativeButton("Cancel",null);
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                });
                break;
            case "experience":
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
//                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });
                break;
            case "selltime":
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));

                switch (Integer.parseInt(mData3.get(position))) {
                    case 1:
                        holder.data3.setText("Available");
                        holder.data3.setTextColor(mContext.getResources().getColor(R.color.colorNephritisGreen));
                        holder.btn1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
//                        Do bid button
//                                Toast.makeText(mContext, "Bid Belum tersedia "+ position, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(mContext, SellTimeBiddingActivity.class);
                                intent.putExtra("id", position);
                                mContext.startActivity(intent);
                            }
                        });
                        break;
                    case 0:
                        holder.data3.setText("Close");
                        holder.data3.setTextColor(Color.GRAY);
                        holder.btn1.setVisibility(View.GONE);
                        break;
                }

                break;
            case "registerexperience":
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.data4.setText(mData4.get(position));
                holder.data5.setText(mData1.get(position));
                holder.exprl.collapse();

                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                            holder.exprl.toggle();
                    }
                });

                holder.btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    //TODO edit button
                        new AlertDialog.Builder(mContext)
                                .setTitle("Edit Experience ?")
                                .setMessage("Do you want to edit your experience ?")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(mContext, RegSellerExperienceAddActivity.class);
                                        intent.putExtra("headline",mData1.get(position));
                                        intent.putExtra("fromDate",mData2.get(position));
                                        intent.putExtra("toDate",mData3.get(position));
                                        intent.putExtra("summary",mData4.get(position));
                                        intent.putExtra("id", position);
                                        fragment.startActivityForResult(intent, 2);
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .show();
                    }
                });

                holder.btn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // TODO delete button
                        new AlertDialog.Builder(mContext)
                                .setTitle("Delete Experience ?")
                                .setMessage("Do you want to delete your experience ?")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (getItemCount()>1) {
                                            mData1.remove(position);
                                            mData2.remove(position);
                                            mData3.remove(position);
                                            mData4.remove(position);
                                            notifyDataSetChanged();
                                        } else {
                                            fragment.getView().findViewById(R.id.textViewNoExperience).setVisibility(View.VISIBLE);
                                            fragment.getView().findViewById(R.id.imageViewNoExperience).setVisibility(View.VISIBLE);
                                            mData1.clear();
                                            mData2.clear();
                                            mData3.clear();
                                            mData4.clear();
                                            notifyDataSetChanged();
                                        }
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .show();
                    }
                });
                break;
            case "expproduct":
                //todo
                holder.data1.setText(mData1.get(position));
                holder.data2.setText("Rp"+mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.data4.setText(mData4.get(position));
                holder.data5.setText(mData1.get(position));
                holder.exprl.collapse();
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.exprl.toggle();
                    }
                });
                holder.btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        new AlertDialog.Builder(mContext)
                                .setTitle("Edit Product ?")
                                .setMessage("Do you want to edit your product ?")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(mContext, RegSellerProductAddActivity.class);
                                        intent.putExtra("id", position);
                                        intent.putExtra("ketproduct", mData1.get(position));
                                        intent.putExtra("harga", mData2.get(position));
                                        intent.putExtra("unitprice", mData3.get(position));
                                        intent.putExtra("note", mData4.get(position));
                                        fragment.startActivityForResult(intent, 5);
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .show();
                    }
                });
                holder.btn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(mContext)
                                .setTitle("Delete Product ?")
                                .setMessage("Do you want to delete your product ?")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (getItemCount()>1) {
                                            mData1.remove(position);
                                            mData2.remove(position);
                                            mData3.remove(position);
                                            mData4.remove(position);
                                            notifyDataSetChanged();
                                        } else {
                                            fragment.getActivity().findViewById(R.id.imageView17).setVisibility(View.VISIBLE);
                                            fragment.getActivity().findViewById(R.id.textView57).setVisibility(View.VISIBLE);
                                            mData1.clear();
                                            mData2.clear();
                                            mData3.clear();
                                            mData4.clear();
                                            notifyDataSetChanged();
                                        }
                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .show();
                    }
                });

                break;
            case "iklan":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image1);
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, SellTimePremium.class);
                        intent.putExtra("id", position);
                        mContext.startActivity(intent);
                    }
                });
                break;
            case "productorder":
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                int i = Integer.parseInt(mData4.get(position));
                holder.spnr1.setSelection(i);
                break;
            case "infotransfer":
                holder.data1.setText(mData1.get(position));
                break;
            case "sellerreview":
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.ratingBar.setRating(Float.valueOf(mratingBar.get(position)));
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image);
                break;
            case "chatdetail":
                switch (holder.getItemViewType()) {
                    case 0:
                        holder.data2.setText(mChatModel.get(position).getPesan());
                        holder.data3.setText(mChatModel.get(position).getWaktu());
                        break;
                    case 1:
                        holder.data1.setText(mChatModel.get(position).getPesan());
                        holder.data4.setText(mChatModel.get(position).getWaktu());
                        break;
                }
                break;
            case "progressbuyerorder":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image);
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.data4.setText(mData4.get(position));
                holder.data5.setText(mData5.get(position));
                holder.data6.setText(mData6.get(position));
                holder.ratingBar.setRating(Float.valueOf(mratingBar.get(position)));
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Toast.makeText(mContext, ""+mData1.get(position), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mContext, BuyTimeProductOrderReviewActivity.class);
                        intent.putExtra("type","progressbuyerorder");
                        mContext.startActivity(intent);
                    }
                });
                break;
            case "profilechatlist":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image);
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Toast.makeText(mContext, mData1.get(position), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mContext,ChatDetailActivity.class);
                        mContext.startActivity(intent);
                    }
                });
                break;
            case "banklist":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image1);
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                break;
            case "historyorder":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image);
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.data4.setText(mData4.get(position));
                holder.data5.setText(mData5.get(position));
                break;
            case "ongoingwork":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mOnGoingModel.get(position).getUrlImage())
                        .into(holder.image);
                holder.data1.setText(mOnGoingModel.get(position).getNama());
                holder.data2.setText(mOnGoingModel.get(position).getProduct());
                holder.data3.setText(mOnGoingModel.get(position).getHarga());
                holder.data4.setText(mOnGoingModel.get(position).getTanggal());
                holder.data5.setText(mOnGoingModel.get(position).getAlamat());
                switch (mOnGoingModel.get(position).getStatus()) {
                    case 0:
                        holder.btn1.setBackgroundResource(R.drawable.btn_orange_filled);
                        holder.btn1.setText("On Going");
                        holder.btn1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mContext, ProgressBuyerOnGoingWorkStatusActivity.class);
                                intent.putExtra("status", "ongoing");
                                mContext.startActivity(intent);
                            }
                        });
                        break;
                    case 1:
                        holder.btn1.setBackgroundResource(R.drawable.btn_login_filled);
                        holder.btn1.setText("Finished");
                        holder.btn1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mContext, ProgressBuyerOnGoingWorkStatusActivity.class);
                                intent.putExtra("status", "finished");
                                mContext.startActivity(intent);
                            }
                        });
                        break;
                }
                break;
            case "orderoffering":
                holder.data6.setVisibility(View.VISIBLE);
                Glide.with(mContext)
                        .asBitmap()
                        .load(mImage.get(position))
                        .into(holder.image);
                holder.data1.setText(mData1.get(position));
                holder.data2.setText(mData2.get(position));
                holder.data3.setText(mData3.get(position));
                holder.data4.setText(mData4.get(position));
                holder.data5.setText(mData5.get(position));
                holder.data6.setText(mData6.get(position));
                holder.layoutParent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext, "test", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(mContext, ProgressSellerOrderBiddingActivity.class);
                        mContext.startActivity(intent);
                    }
                });
                break;
            case "owforseller":
                Glide.with(mContext)
                        .asBitmap()
                        .load(mOnGoingModel.get(position).getUrlImage())
                        .into(holder.image);
                holder.data1.setText(mOnGoingModel.get(position).getNama());
                holder.data2.setText(mOnGoingModel.get(position).getProduct());
                holder.data3.setText(mOnGoingModel.get(position).getHarga());
                holder.data4.setText(mOnGoingModel.get(position).getTanggal());
                holder.data5.setText(mOnGoingModel.get(position).getAlamat());
                switch (mOnGoingModel.get(position).getStatus()) {
                    case 0:
                        holder.btn1.setBackgroundResource(R.drawable.btn_orange_filled);
                        holder.btn1.setText("On Going");
                        holder.btn1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mContext, ProgressBuyerOnGoingWorkStatusActivity.class);
                                intent.putExtra("status", "owforseller");
                                mContext.startActivity(intent);
//                                ((Activity) mContext).finish();
                            }
                        });
                        break;
                }
                break;
        }
    }



    @Override
    public int getItemCount() {
        int size;
                switch (jenis) {
                    case "chatdetail":
                        size = mChatModel.size();
                        break;
                    case "feedback":
                        size = mImage.size();
                        break;
                    case "iklan":
                        size = mImage.size();
                        break;
                    case "ongoingwork":
                        size = mOnGoingModel.size();
                        break;
                    case "owforseller":
                        size = mOnGoingModel.size();
                        break;
                    default:
                size = mData1.size();
            break;
        }
        return size;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        String string1;
        Button btn1,btn2;
        ImageView image1;
        CircleImageView image;
        TextView data1,data2,data3,data4,data5,data6;
        Spinner spnr1;
        RatingBar ratingBar;
        ConstraintLayout layoutParent;
        TextInputEditText tiet1,tiet2,tiet3,tiet4;
        ExpandableRelativeLayout exprl;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            switch (jenis) {
                case "listitem":
                    layoutParent = itemView.findViewById(R.id.layout_message);
                    image = itemView.findViewById(R.id.circleImageView);
                    data1 = itemView.findViewById(R.id.textView_nama);
                    data2 = itemView.findViewById(R.id.textView_waktu);
                    break;
                case "listjeniskeahlian":
                    layoutParent = itemView.findViewById(R.id.layout_jeniskeahlian);
                    data1 = itemView.findViewById(R.id.textView11);
                    data2 = itemView.findViewById(R.id.textView12);
                    data3 = itemView.findViewById(R.id.textView13);
                    break;
                case "buytime":
                    layoutParent = itemView.findViewById(R.id.layout_buytime);
                    image = itemView.findViewById(R.id.circleImageView_buyTime);
                    data1 = itemView.findViewById(R.id.textView_Name);
                    data2 = itemView.findViewById(R.id.textView_Lokasi);
                    data3 = itemView.findViewById(R.id.textView_totalRating);
                    data4 = itemView.findViewById(R.id.textView_pengalaman);
                    data5 = itemView.findViewById(R.id.textView_profesi);
                    data6 = itemView.findViewById(R.id.textView_harga);
                    ratingBar = itemView.findViewById(R.id.ratingBar);
                    break;
                case "review":
                    layoutParent = itemView.findViewById(R.id.layout_review_postorder);
                    image = itemView.findViewById(R.id.circleImageView2);
                    data1 = itemView.findViewById(R.id.textView_name);
                    data2 = itemView.findViewById(R.id.textView_loc);
                    data3 = itemView.findViewById(R.id.textView_expert);
                    data4 = itemView.findViewById(R.id.textView_amountOrder);
                    data5 = itemView.findViewById(R.id.textView_price);
                    data6 = itemView.findViewById(R.id.textView_ratingAmount);
                    ratingBar = itemView.findViewById(R.id.ratingBar);
                    break;
                case "feedback":
                    layoutParent = itemView.findViewById(R.id.layout_parent);
                    image1 = itemView.findViewById(R.id.imageView21);
                    break;
                case "bankaccount":
                    btn1 = itemView.findViewById(R.id.button_edit);
                    btn2 = itemView.findViewById(R.id.button_hapus);
                    layoutParent = itemView.findViewById(R.id.layout_utama);
                    image1 = itemView.findViewById(R.id.imageView_logo);
                    data1 = itemView.findViewById(R.id.textView_bankName);
                    data2 = itemView.findViewById(R.id.textView_accountName);
                    data3 = itemView.findViewById(R.id.textView_bankNumber);
                    break;
                case "experience":
                    layoutParent = itemView.findViewById(R.id.layout_parent);
                    data1 = itemView.findViewById(R.id.textView_title);
                    data2 = itemView.findViewById(R.id.textView_tanggal);
                    data3 = itemView.findViewById(R.id.textView_desc);
                    break;
                case "selltime":
                    layoutParent = itemView.findViewById(R.id.mainParent);
                    data1 = itemView.findViewById(R.id.textViewTitle);
                    data2 = itemView.findViewById(R.id.textViewTime);
                    data3 = itemView.findViewById(R.id.textViewStatus);
                    btn1 = itemView.findViewById(R.id.buttonBid);
                    break;
                case "registerexperience":
                    exprl = itemView.findViewById(R.id.content);
                    layoutParent = itemView.findViewById(R.id.head);
                    data1 = itemView.findViewById(R.id.headline);
                    data2 = itemView.findViewById(R.id.fromDate);
                    data3 = itemView.findViewById(R.id.toDate);
                    data4 = itemView.findViewById(R.id.summary);
                    data5 = itemView.findViewById(R.id.textView18);
                    btn1 = itemView.findViewById(R.id.button6);
                    btn2 = itemView.findViewById(R.id.button7);
                    break;
                case "expproduct":
                    exprl = itemView.findViewById(R.id.content);
                    layoutParent = itemView.findViewById(R.id.head);
                    data1 = itemView.findViewById(R.id.textView21);
                    data2 = itemView.findViewById(R.id.textView46);
                    data3 = itemView.findViewById(R.id.textView55);
                    data4 = itemView.findViewById(R.id.textView56);
                    data5 = itemView.findViewById(R.id.textView18);
                    btn1 = itemView.findViewById(R.id.btnEdit2);
                    btn2 = itemView.findViewById(R.id.btnHapus2);
                    break;
                case "iklan":
                    layoutParent = itemView.findViewById(R.id.layout_parent);
                    image1 = itemView.findViewById(R.id.imageView21);
                    break;
                case "productorder":
                    data1 = itemView.findViewById(R.id.textView95);
                    data2 = itemView.findViewById(R.id.textView97);
                    data3 = itemView.findViewById(R.id.notes);
                    spnr1 = itemView.findViewById(R.id.spinner11);
                    break;
                case "infotransfer":
                    data1 = itemView.findViewById(R.id.textView106);
                    break;
                case "sellerreview":
                    data1 = itemView.findViewById(R.id.textView107);
                    data2 = itemView.findViewById(R.id.textView108);
                    data3 = itemView.findViewById(R.id.textView109);
                    ratingBar = itemView.findViewById(R.id.ratingBar2);
                    image = itemView.findViewById(R.id.circleImageView5);
                    break;
                case "chatdetail":
                    data1 = itemView.findViewById(R.id.textView113);
                    data2 = itemView.findViewById(R.id.textView112);
                    data3 = itemView.findViewById(R.id.textView114);
                    data4 = itemView.findViewById(R.id.textView115);
                    break;
                case "progressbuyerorder":
                    layoutParent = itemView.findViewById(R.id.layout_review_postorder);
                    image = itemView.findViewById(R.id.circleImageView2);
                    data1 = itemView.findViewById(R.id.textView_name);
                    data2 = itemView.findViewById(R.id.textView_loc);
                    data3 = itemView.findViewById(R.id.textView_expert);
                    data4 = itemView.findViewById(R.id.textView_amountOrder);
                    data5 = itemView.findViewById(R.id.textView_price);
                    data6 = itemView.findViewById(R.id.textView_ratingAmount);
                    ratingBar = itemView.findViewById(R.id.ratingBar);
                    break;
                case "profilechatlist":
                    layoutParent = itemView.findViewById(R.id.layout_message);
                    image = itemView.findViewById(R.id.circleImageView);
                    data1 = itemView.findViewById(R.id.textView_nama);
                    data2 = itemView.findViewById(R.id.textView_waktu);
                    break;
                case "banklist":
                    layoutParent = itemView.findViewById(R.id.layoututama);
                    image1 = itemView.findViewById(R.id.imageView20);
                    data1 = itemView.findViewById(R.id.textView121);
                    data2 = itemView.findViewById(R.id.textView123);
                    break;
                case "historyorder":
                    image = itemView.findViewById(R.id.circleImageView9);
                    data1 = itemView.findViewById(R.id.textView124);
                    data2 = itemView.findViewById(R.id.textView125);
                    data3 = itemView.findViewById(R.id.textView126);
                    data4 = itemView.findViewById(R.id.textView127);
                    data5 = itemView.findViewById(R.id.textView128);
                    break;
                case "ongoingwork":
                    image = itemView.findViewById(R.id.circleImageView10);
                    data1 = itemView.findViewById(R.id.textView129);
                    data2 = itemView.findViewById(R.id.textView130);
                    data3 = itemView.findViewById(R.id.textView131);
                    data4 = itemView.findViewById(R.id.textView132);
                    data5 = itemView.findViewById(R.id.textView133);
                    btn1 = itemView.findViewById(R.id.button21);
                    break;
                case "orderoffering":
                    layoutParent = itemView.findViewById(R.id.parent);
                    image = itemView.findViewById(R.id.circleImageView9);
                    data1 = itemView.findViewById(R.id.textView124);
                    data2 = itemView.findViewById(R.id.textView125);
                    data3 = itemView.findViewById(R.id.textView126);
                    data4 = itemView.findViewById(R.id.textView127);
                    data5 = itemView.findViewById(R.id.textView128);
                    data6 = itemView.findViewById(R.id.textView136);
                    break;
                case "owforseller":
                    image = itemView.findViewById(R.id.circleImageView10);
                    data1 = itemView.findViewById(R.id.textView129);
                    data2 = itemView.findViewById(R.id.textView130);
                    data3 = itemView.findViewById(R.id.textView131);
                    data4 = itemView.findViewById(R.id.textView132);
                    data5 = itemView.findViewById(R.id.textView133);
                    btn1 = itemView.findViewById(R.id.button21);
                    break;
            }
        }
    }
}
