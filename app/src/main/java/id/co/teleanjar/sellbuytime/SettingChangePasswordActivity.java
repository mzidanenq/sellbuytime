package id.co.teleanjar.sellbuytime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class SettingChangePasswordActivity extends AppCompatActivity {
    Button save;
    Intent intent;
    Toolbar toolbar;
    TextView confirminfo,checkedInfo;
    Boolean saving;
    ProgressBar pgBar;
    EditText oldPass,newpass,confirmpass;
    TextInputLayout tilOld,tilNew,tilConfirm;
    ImageView checked;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_change_password);
        toolbar = findViewById(R.id.toolbar_resetpass);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Change Password");

        save = findViewById(R.id.button_save);
        oldPass = findViewById(R.id.editText_oldPassword);
        newpass = findViewById(R.id.editText_newPassword);
        confirmpass = findViewById(R.id.editText_confirmPassword);
        confirminfo = findViewById(R.id.textView39);

        checkedInfo = findViewById(R.id.textView40);
        checked = findViewById(R.id.imageView14);
        pgBar = findViewById(R.id.progressBar);


        tilNew = findViewById(R.id.tilNewPass);
        tilOld = findViewById(R.id.tilOldPass);
        tilConfirm = findViewById(R.id.tilConfirmPass);

        tilNew.setVisibility(View.GONE);
        tilConfirm.setVisibility(View.GONE);
        save.setVisibility(View.GONE);

        oldPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (oldPass.getText().length()<6) {
                    oldPass.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_close_red,0);
                } else {
                    oldPass.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_check_green,0);
                    oldPass.setBackgroundTintList(ContextCompat.getColorStateList(SettingChangePasswordActivity.this,R.color.colorPrimary));
                    tilOld.setDefaultHintTextColor(ContextCompat.getColorStateList(SettingChangePasswordActivity.this,R.color.colorPrimary));
                    tilNew.setVisibility(View.VISIBLE);
                }
            }
        });

        newpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (newpass.getText().length()<6 ) {
                    newpass.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_close_red,0);
                } else {
                    newpass.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_check_green,0);
                    newpass.setBackgroundTintList(ContextCompat.getColorStateList(SettingChangePasswordActivity.this,R.color.colorPrimary));
                    tilNew.setDefaultHintTextColor(ContextCompat.getColorStateList(SettingChangePasswordActivity.this,R.color.colorPrimary));
                    tilConfirm.setVisibility(View.VISIBLE);
                }
            }
        });

        confirmpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(confirmpass.getText().toString().equals(newpass.getText().toString()) && confirmpass.getText().length()>=6) {
                        confirmpass.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_check_green,0);
                        confirmpass.setBackgroundTintList(ContextCompat.getColorStateList(SettingChangePasswordActivity.this,R.color.colorPrimary));
                        tilConfirm.setDefaultHintTextColor(ContextCompat.getColorStateList(SettingChangePasswordActivity.this,R.color.colorPrimary));
                        save.setVisibility(View.VISIBLE);
                        confirminfo.setVisibility(View.GONE);
                        saving = true;
                    } else {
                        confirmpass.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_close_red,0);
                        confirmpass.setBackgroundTintList(ContextCompat.getColorStateList(SettingChangePasswordActivity.this,R.color.colorAccent));
                        confirminfo.setVisibility(View.VISIBLE);
                        saving = false;
//                        ini di comment selama proses development untuk mencoba fitur error
//                        save.setVisibility(View.GONE);
                    }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //do your logic
                if (saving) {
                    pgBar.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pgBar.setVisibility(View.GONE);
                            checkedInfo.setVisibility(View.VISIBLE);
                            checked.setVisibility(View.VISIBLE);
                        }
                    }, 2000);
                } else {
                    pgBar.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pgBar.setVisibility(View.GONE);
                            checked.setImageResource(R.drawable.ic_close_red);
                            checkedInfo.setTextColor(getResources().getColor(R.color.colorAccent));
                            checkedInfo.setText("Save Password Failed");
                        }
                    },2000);

                }

//                finish();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
