package id.co.teleanjar.sellbuytime;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SellTimeBiddingActivity extends AppCompatActivity {
    Button bto;
    TextView tv1;
    EditText amount;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_time_bidding);
        bto = findViewById(R.id.button13);
        tv1 = findViewById(R.id.textView58);
        amount = findViewById(R.id.editText);
        toolbar = findViewById(R.id.toolbar10);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Wait a Seconds...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        bto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.getProgress();
                progressDialog.show();
                amount.clearFocus();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        showDialogFragment();
                    }
                },5000);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void showDialogFragment() {
        FragmentManager fm = getSupportFragmentManager();
        String message = "Terima kasih \n anda telah melakukan bidding atas pekerjaan ini. Silahkan menunggu tanggapan buyer, notifikasi acceptance / decline silahkan cek menu \"progress\" dan email anda ";
        DialogFragment df = DialogFragment.newInstance(message,"satu");
        df.show(fm,"fragment_dialog");
    }
}
