package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;
import id.co.teleanjar.sellbuytime.model.ChatModel;

public class ChatDetailActivity extends AppCompatActivity {
    TextView mName,mTime;
    ImageView mSent;
    TextView mTypeMessage;
    RecyclerView mChatMessage;
    RecyclerViewAdapter recyclerViewAdapter;
    Toolbar toolbar;
    ArrayList<ChatModel> msg = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_detail);
        mName = findViewById(R.id.textView110);
        mTime = findViewById(R.id.textView111);
        mSent = findViewById(R.id.imageView18);
        mTypeMessage = findViewById(R.id.editText13);
        mChatMessage = findViewById(R.id.chatMessage);
        toolbar = findViewById(R.id.toolbar18);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        DateFormat dfc = new SimpleDateFormat("h:mm a");
        String clock = dfc.format(Calendar.getInstance().getTime());
        mTime.setText("last seen today at "+clock);

//        Date currentTime = Calendar.getInstance().getTime();
        msg.add(new ChatModel("pembeli","Apakah stock tersedia ?",clock));
        msg.add(new ChatModel("penjual","banyak kok ?",clock));

        recyclerViewAdapter = new RecyclerViewAdapter("chatdetail",this,msg);
        mChatMessage.setLayoutManager(new LinearLayoutManager(this));
        mChatMessage.setAdapter(recyclerViewAdapter);

        mSent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mTypeMessage.getText().toString().equals("")) {
                    DateFormat dfc = new SimpleDateFormat("h:mm a");
                    String clock = dfc.format(Calendar.getInstance().getTime());
                    int index = recyclerViewAdapter.getItemCount();
                    msg.add(new ChatModel("pembeli",mTypeMessage.getText().toString(),clock));
                    recyclerViewAdapter.notifyItemInserted(index);
                    mChatMessage.scrollToPosition(index);
                    if (mTypeMessage.getText().toString().equals("p")) {
                        int index2 = recyclerViewAdapter.getItemCount();
                        recyclerViewAdapter.notifyItemInserted(index2);
                        msg.add(new ChatModel("penjual","ya ?",clock));
                        mChatMessage.scrollToPosition(index2);
                    }
                    mTypeMessage.setText("");
                } else {
                    Toast.makeText(ChatDetailActivity.this, "Message can't empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
