package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kofigyan.stateprogressbar.StateProgressBar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_FIRST_USER;
import static android.app.Activity.RESULT_OK;


public class RegSellerExperienceFragment extends Fragment {
    RecyclerViewAdapter recyclerViewAdapter;
    RecyclerView recyclerView;
    Button btnSave,btnAdd;
    ArrayList<String> headline = new ArrayList<>();
    ArrayList<String> ToDate = new ArrayList<>();
    ArrayList<String> FromDate = new ArrayList<>();
    ArrayList<String> summary = new ArrayList<>();
    int id;

    ImageView IvNoExperience;
    TextView TvNoExperience;

    public RegSellerExperienceFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reg_seller_experience, container, false);

        btnSave = v.findViewById(R.id.btnSave);
        btnAdd = v.findViewById(R.id.button16);
        recyclerView = v.findViewById(R.id.recyclerViews);
        IvNoExperience = v.findViewById(R.id.imageViewNoExperience);
        TvNoExperience = v.findViewById(R.id.textViewNoExperience);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), RegSellerExperienceAddActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo save data experience
                ((RegSellerActivity)getActivity()).viewPager.setCurrentItem(3,true);
                ((RegSellerActivity)getActivity()).stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
            }
        });
        return v;
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("registerexperience",getContext(),this,headline,FromDate,ToDate,summary,null,null);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        id = data.getIntExtra("id",-1);
        if (requestCode == 1 && resultCode == RESULT_FIRST_USER) {
            TvNoExperience.setVisibility(View.GONE);
            IvNoExperience.setVisibility(View.GONE);
            headline.add(data.getStringExtra("headline"));
            ToDate.add(data.getStringExtra("toDate"));
            FromDate.add(data.getStringExtra("fromDate"));
            summary.add(data.getStringExtra("summary"));
        } else if(resultCode == RESULT_CANCELED) {
            Toast.makeText(getContext(), "Add experience canceled", Toast.LENGTH_SHORT).show();
        } else if (requestCode == 2 && resultCode == RESULT_OK) {
            headline.set(id,data.getStringExtra("headline"));
            ToDate.set(id,data.getStringExtra("toDate"));
            FromDate.set(id,data.getStringExtra("fromDate"));
            summary.set(id,data.getStringExtra("summary"));
            recyclerViewAdapter.notifyItemChanged(id);
        }
        initRecyclerView();
    }
}
