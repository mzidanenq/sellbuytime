package id.co.teleanjar.sellbuytime.lib;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class LockableViewPager extends ViewPager {
    boolean swipeable;
    public LockableViewPager(@NonNull Context context) {
        super(context);
    }

    public LockableViewPager(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        this.swipeable = true;
    }


    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (this.swipeable) {
            return super.onTouchEvent(ev);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (this.swipeable) {
            return super.onInterceptTouchEvent(ev);
        } return false;
    }

    public void setSwipeable(boolean swipeable) {
        this.swipeable = swipeable;
    }
}
