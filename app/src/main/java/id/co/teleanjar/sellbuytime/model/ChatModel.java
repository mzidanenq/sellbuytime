package id.co.teleanjar.sellbuytime.model;

public class ChatModel {
    private Integer id;
    private String user;
    private String pesan;
    private String waktu;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        id = 0;
        this.id = id;
        id++;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public ChatModel(String user, String pesan, String waktu) {
        this.user = user;
        this.pesan = pesan;
        this.waktu = waktu;
    }
}
