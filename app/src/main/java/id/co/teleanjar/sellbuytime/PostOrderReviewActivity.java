package id.co.teleanjar.sellbuytime;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class PostOrderReviewActivity extends AppCompatActivity {
    Toolbar toolbar;
    Button mChange,mPostOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_order_review);
        toolbar = findViewById(R.id.toolbar13);
        mChange = findViewById(R.id.button18);
        mPostOrder = findViewById(R.id.button17);
        setSupportActionBar(toolbar);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Wait a Seconds...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPostOrder.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               progressDialog.getProgress();
               progressDialog.show();
               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       progressDialog.dismiss();
                       showDialogFragment();
                   }
               },5000);

           }
       });

      mChange.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });
    }

    void showDialogFragment() {
        FragmentManager fm = getSupportFragmentManager();
        String message = "Terima kasih \n Order Anda telah diteruskan kepada Seller (Penyedia Jasa) Silahkan menunggu tanggapan Seller, notifikasi acceptance / decline silahkan cek menu \"Progress\" dan email Anda \n Informasi kebutuhan pekerjaan Anda dapat dilihat pada halaman Job List ";
        DialogFragment df = DialogFragment.newInstance(message,"satu");
        df.show(fm,"fragment_dialog");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
