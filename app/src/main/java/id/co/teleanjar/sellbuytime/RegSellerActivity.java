package id.co.teleanjar.sellbuytime;

import android.app.ProgressDialog;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kofigyan.stateprogressbar.StateProgressBar;
import com.kofigyan.stateprogressbar.components.StateItem;
import com.kofigyan.stateprogressbar.listeners.OnStateItemClickListener;

import id.co.teleanjar.sellbuytime.lib.LockableViewPager;
import id.co.teleanjar.sellbuytime.lib.SectionsPagerAdapter;

public class RegSellerActivity extends AppCompatActivity {
    Toolbar toolbar;
    ViewPager viewPager;
    SectionsPagerAdapter sectionsPagerAdapter;
    String[] descriptionData = {"Profile", "Skill", "Experience", "Product"};
    StateProgressBar stateProgressBar;
    ProgressBar progressBar;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_seller);
        toolbar = findViewById(R.id.toolbar);
        viewPager = (LockableViewPager) findViewById(R.id.viewPager);
        stateProgressBar = findViewById(R.id.stateProgressBar);
        setSupportActionBar(toolbar);
        ((LockableViewPager) viewPager).setSwipeable(false);
        progressBar = findViewById(R.id.progressBar2);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Registering your account.");
        progressDialog.setTitle("Loading...");
        progressDialog.setProgressStyle(progressDialog.STYLE_SPINNER);

        stateProgressBar.setOnStateItemClickListener(new OnStateItemClickListener() {
            @Override
            public void onStateItemClick(StateProgressBar stateProgressBar, StateItem stateItem, int stateNumber, boolean isCurrentState) {
                switch (stateProgressBar.getCurrentStateNumber()) {
                    case 1:
                        if (isCurrentState) {
                            Toast.makeText(RegSellerActivity.this, "You're in current step", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(RegSellerActivity.this, "You can't bypass this step", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 2:
                        switch (stateNumber) {
                            case 1:
                                Toast.makeText(RegSellerActivity.this, "You're in step "+stateNumber, Toast.LENGTH_SHORT).show();
                                viewPager.setCurrentItem(0, true);
                                break;
                            case 2:
                                Toast.makeText(RegSellerActivity.this, "You're in current step", Toast.LENGTH_SHORT).show();
                                viewPager.setCurrentItem(1, true);
                                break;
                            default:
                                Toast.makeText(RegSellerActivity.this, "You can't bypass this step", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 3:
                        switch (stateNumber) {
                            case 1:
                                Toast.makeText(RegSellerActivity.this, "You're in step "+stateNumber, Toast.LENGTH_SHORT).show();
                                viewPager.setCurrentItem(0, true);
                                break;
                            case 2:
                                Toast.makeText(RegSellerActivity.this, "You're in step "+stateNumber, Toast.LENGTH_SHORT).show();
                                viewPager.setCurrentItem(1, true);
                                break;
                            case 3:
                                Toast.makeText(RegSellerActivity.this, "You're in current step", Toast.LENGTH_SHORT).show();
                                viewPager.setCurrentItem(2, true);
                                break;
                            default:
                                Toast.makeText(RegSellerActivity.this, "You can't bypass this step", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case 4:
                        switch (stateNumber) {
                            case 1:
                                viewPager.setCurrentItem(0,true);
                                Toast.makeText(RegSellerActivity.this, "You're in step "+stateNumber, Toast.LENGTH_SHORT).show();
                                break;
                            case 2:
                                Toast.makeText(RegSellerActivity.this, "You're in step "+stateNumber, Toast.LENGTH_SHORT).show();
                                viewPager.setCurrentItem(1,true);
                                break;
                            case 3:
                                Toast.makeText(RegSellerActivity.this, "You're in step "+stateNumber, Toast.LENGTH_SHORT).show();
                                viewPager.setCurrentItem(2,true);
                                break;
                            case 4:
                                Toast.makeText(RegSellerActivity.this, "You're in current step", Toast.LENGTH_SHORT).show();
                                viewPager.setCurrentItem(3,true);
                                break;
                        }
                        break;
                }
            }
        });

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sectionsPagerAdapter.AddFragment(new RegSellerProfileFragment(), null);
        sectionsPagerAdapter.AddFragment(new RegSellerSkillFragment(), null);
        sectionsPagerAdapter.AddFragment(new RegSellerExperienceFragment(), null);
        sectionsPagerAdapter.AddFragment(new RegSellerProductFragment(), null);
        viewPager.setAdapter(sectionsPagerAdapter);
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
