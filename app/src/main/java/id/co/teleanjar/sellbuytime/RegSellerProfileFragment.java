package id.co.teleanjar.sellbuytime;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.icu.text.Replaceable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kofigyan.stateprogressbar.StateProgressBar;

import java.io.File;

import static android.app.Activity.RESULT_OK;

public class RegSellerProfileFragment extends Fragment {
    Button btnSave,btnBrowse;
    ImageView photoProfile;
    TextView textViewPhoto;
    TextInputEditText ktpName;
    static final int PICK_IMAGE = 100;
    Uri imageUri;
    String type;

    public RegSellerProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reg_seller_profile, container, false);

        btnSave = view.findViewById(R.id.buttonSave);
        btnBrowse = view.findViewById(R.id.buttonBrowse);
        photoProfile = view.findViewById(R.id.imageViewPhotoProfile);
        textViewPhoto = view.findViewById(R.id.textViewPhoto);
        ktpName = view.findViewById(R.id.imageKTPName);

        photoProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery("profile");
            }
        });

        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery("ktp");
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Registrasi Seller ?")
                        .setMessage("Anda ingin melanjutkan pendaftaran menjadi seller ?. jika klik batal maka anda hanya melengkapi profile buyer saja.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ((RegSellerActivity)getActivity()).viewPager.setCurrentItem(1,true);
                                ((RegSellerActivity)getActivity()).stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                            }
                        })
                        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // save profile
                                getActivity().finish();
                            }
                        })
                        .show();
            }
        });
        return view;
    }

    void openGallery(String type) {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        this.type = type;

        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            switch (type) {
                case "profile":
                    textViewPhoto.setVisibility(View.GONE);
                    imageUri = data.getData();
                    String picturePath = getPath(getActivity().getApplicationContext(), imageUri);
                    String filename=picturePath.substring(picturePath.lastIndexOf("/")+1);
//                    textViewPhoto.setText(filename);
                    photoProfile.setImageURI(imageUri);
                    break;
                case "ktp":
                    imageUri = data.getData();
                    String picturePath2 = getPath(getActivity().getApplicationContext(), imageUri);
                    String filename2=picturePath2.substring(picturePath2.lastIndexOf("/")+1);
                    ktpName.setText(filename2);
                    break;
            }
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri,proj,null,null,null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
            if (result == null) {
                result = "Not Found";
            }
        }
        return result;
    }
}
