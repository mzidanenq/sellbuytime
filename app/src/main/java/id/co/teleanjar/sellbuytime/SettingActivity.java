package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class SettingActivity extends AppCompatActivity {
    Intent intent;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        toolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        getWindow().getDecorView().setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
//                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//        );
    }

    public void clickChangePassword(View view) {
        intent = new Intent(SettingActivity.this,SettingChangePasswordActivity.class);
        startActivity(intent);
    }

    public void clickEditProfile(View view) {
        intent = new Intent(SettingActivity.this, SettingEditProfileActivity.class);
        startActivity(intent);
    }

    public void clickBankAccount(View view) {
        intent = new Intent(SettingActivity.this, SettingBankAccountActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
