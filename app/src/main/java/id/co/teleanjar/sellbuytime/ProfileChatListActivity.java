package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class ProfileChatListActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView chatList;
    RecyclerViewAdapter adapter;
    ArrayList<String> mNama = new ArrayList<>();
    ArrayList<String> mImageLink = new ArrayList<>();
    ArrayList<String> mWaktu = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_chat_list);
        toolbar = findViewById(R.id.toolbar20);
        chatList = findViewById(R.id.recyclerViews);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        iniData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initRecyclerView() {
        RecyclerViewAdapter adapter = new RecyclerViewAdapter("profilechatlist",this,mImageLink, mNama,mWaktu,null,null,null,null,null);
        chatList.setAdapter(adapter);
        chatList.setLayoutManager(new LinearLayoutManager(this));
    }

    private void iniData() {

        mImageLink.add("https://i.pinimg.com/564x/cd/32/71/cd3271c3c4e82d84c25039c94dfa8004.jpg");
        mNama.add("Moschino lorem ipsum dolor si amet");
        mWaktu.add("12/12/2012. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/83/64/af/8364afc2d9cc7cde7010c8004a1f8820.jpg");
        mNama.add("Très chic");
        mWaktu.add("12/12/2013. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/7a/6b/d4/7a6bd4465d9c28213edb2bcae4a549bc.jpg");
        mNama.add("Ms. Monroe");
        mWaktu.add("12/12/2014. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/5f/c3/e9/5fc3e9170d96991b51c34bdf99800061.jpg");
        mNama.add("edgy");
        mWaktu.add("12/12/2012. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/37/73/52/377352c489acb98643d50de2762de7b5.jpg");
        mNama.add("Joe Scarborough");
        mWaktu.add("12/12/2012. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/a4/8c/e6/a48ce618b2226637eb9f8c4d7d4db9de.jpg");
        mNama.add("Twiggy");
        mWaktu.add("12/12/2012. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/48/f7/83/48f7834dd8ad72c9162919fbbf913c75.jpg");
        mNama.add("John Doe");
        mWaktu.add("12/12/2012. 12:12");

        initRecyclerView();
    }

}
