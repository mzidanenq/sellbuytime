package id.co.teleanjar.sellbuytime.model;

public class OnGoingModel {
    private Integer status;
    private String urlImage;
    private String nama;
    private String product;
    private String harga;
    private String tanggal;
    private String alamat;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public OnGoingModel(Integer status, String urlImage, String nama, String product, String harga, String tanggal, String alamat) {
        this.status = status;
        this.urlImage = urlImage;
        this.nama = nama;
        this.product = product;
        this.harga = harga;
        this.tanggal = tanggal;
        this.alamat = alamat;
    }
}
