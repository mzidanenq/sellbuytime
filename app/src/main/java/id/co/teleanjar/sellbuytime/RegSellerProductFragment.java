package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_FIRST_USER;
import static android.app.Activity.RESULT_OK;


public class RegSellerProductFragment extends Fragment {
    Button btnSave,mBtnAdd;
    ConstraintLayout constraintLayout;
    RecyclerViewAdapter recyclerViewAdapter;
    RecyclerView recyclerView;
    ArrayList<String> mKetProduct = new ArrayList<>();
    ArrayList<String> mHarga = new ArrayList<>();
    ArrayList<String> mUnitPrice = new ArrayList<>();
    ArrayList<String> mNote = new ArrayList<>();
    Intent intent;
    Integer id;
    ImageView IvNoProduct;
    TextView TvNoProduct;


    public RegSellerProductFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_reg_seller_product, container, false);
        btnSave = v.findViewById(R.id.btnSave);
        mBtnAdd = v.findViewById(R.id.btnAdd);
        recyclerView = v.findViewById(R.id.recyclerViewss);
        constraintLayout = v.findViewById(R.id.parent_layout);
        IvNoProduct = v.findViewById(R.id.imageView17);
        TvNoProduct = v.findViewById(R.id.textView57);

        mBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO ADD ACTION BUTTON ADD IN REGSELLER PRODUCT
                intent = new Intent(getContext(), RegSellerProductAddActivity.class);
                startActivityForResult(intent, 4);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO ADD ACTION BUTTON SAVE IN REGSELLER PRODUCT

                ((RegSellerActivity)getActivity()).stateProgressBar.setAllStatesCompleted(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Registration Successfull", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                },2000);
                ((RegSellerActivity)getActivity()).progressDialog.getProgress();
                ((RegSellerActivity)getActivity()).progressDialog.show();
            }
        });
        return v;
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("expproduct",getContext(),this,mKetProduct,mHarga,mUnitPrice,mNote,null,null);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        id = data.getIntExtra("id", -1);
        if (requestCode == 4 && resultCode == RESULT_FIRST_USER) {
            mKetProduct.add(data.getStringExtra("ketproduct"));
            mHarga.add(data.getStringExtra("harga"));
            mUnitPrice.add(data.getStringExtra("unitprice"));
            mNote.add(data.getStringExtra("note"));
            IvNoProduct.setVisibility(View.GONE);
            TvNoProduct.setVisibility(View.GONE);
        } else if (requestCode == 5 && resultCode == RESULT_OK) {
            mKetProduct.set(id,data.getStringExtra("ketproduct"));
            mHarga.set(id,data.getStringExtra("harga"));
            mUnitPrice.set(id,data.getStringExtra("unitprice"));
            mNote.set(id,data.getStringExtra("note"));
            recyclerViewAdapter.notifyItemChanged(id);
        } else if(resultCode == RESULT_CANCELED) {
            Toast.makeText(getContext(), "Add Product Canceled", Toast.LENGTH_SHORT).show();
        }
        initRecyclerView();
    }
}
