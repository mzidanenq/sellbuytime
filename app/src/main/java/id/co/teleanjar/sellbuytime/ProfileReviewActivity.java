package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class ProfileReviewActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    ArrayList<String> mImage = new ArrayList<>();
    ArrayList<String> mNama = new ArrayList<>();
    ArrayList<String> mKode = new ArrayList<>();
    ArrayList<String> mUlasan = new ArrayList<>();
    ArrayList<String> mRating = new ArrayList<>();
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_review);
        recyclerView = findViewById(R.id.recyclerviews);
        toolbar = findViewById(R.id.toolbar17);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initData();
    }
    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("sellerreview",this,mImage,mNama,mKode,mUlasan,null,null,null,mRating);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    void initData() {
        for (int i = 0; i<10;i++) {
            mImage.add("https://cdn.business2community.com/wp-content/uploads/2014/04/profile-picture-300x300.jpg");
            mNama.add("Bradley M. Ball");
            mKode.add("123/12212212/EE/2E/21ED2");
            mUlasan.add("Pelayanan sangat baik");
            mRating.add("3.5");
        }

        initRecyclerView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
