package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class    SellTimeActivity extends AppCompatActivity {
    RecyclerView mRecyclerView, iklan;
    RecyclerViewAdapter recyclerViewAdapter, recyclerViewAdapter2;
    Toolbar toolbar;

    ArrayList<String> mImage = new ArrayList<>();
    ArrayList<String> title = new ArrayList<>();
    ArrayList<String> time = new ArrayList<>();
    ArrayList<String> status = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_time);
        mRecyclerView = findViewById(R.id.recyclerViewss);
        iklan = findViewById(R.id.iklan);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initData();
        initData2();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();

        return true;
    }

    void initRecyclerView2() {
        recyclerViewAdapter2 = new RecyclerViewAdapter("iklan", this,mImage,null,null,null,null,null,null,null);
        iklan.setLayoutManager(new GridLayoutManager(this,2,GridLayoutManager.HORIZONTAL,false));
        iklan.setAdapter(recyclerViewAdapter2);
    }

    void initData2() {
        for (int i=0; i<24; i++) {
            mImage.add("https://www.nouveau.co.uk/wp/wp-content/uploads/2017/08/we-are-hiring-image-e1502452302975.jpg");
        }
        initRecyclerView2();
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("selltime",this,null,title,time,status,null,null,null,null);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(recyclerViewAdapter);
    }

    void initData() {
            title.add("Teknisi AC, Membersihkan AC Window 2 unit, Depok");
            time.add("21/8/2018, 10:00 AM");
            status.add("1");

            title.add("Jasa Aqiqah, Aqiqah 1 ekor kambing, sudirman.");
            time.add("3/9/2018, 11:00 AM");
            status.add("0");
          title.add("Teknisi AC, Membersihkan AC Window 2 unit, Depok");
            time.add("21/8/2018, 10:00 AM");
            status.add("1");

            title.add("Jasa Aqiqah, Aqiqah 1 ekor kambing, sudirman.");
            time.add("3/9/2018, 11:00 AM");
            status.add("0");
          title.add("Teknisi AC, Membersihkan AC Window 2 unit, Depok");
            time.add("21/8/2018, 10:00 AM");
            status.add("1");

            title.add("Jasa Aqiqah, Aqiqah 1 ekor kambing, sudirman.");
            time.add("3/9/2018, 11:00 AM");
            status.add("0");


        initRecyclerView();
    }
}
