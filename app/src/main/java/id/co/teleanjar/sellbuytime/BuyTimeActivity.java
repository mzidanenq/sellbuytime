package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class BuyTimeActivity extends AppCompatActivity {
    RecyclerViewAdapter adapter;
    RecyclerView recyclerView;
    Toolbar toolbar;
    ArrayList<String> image = new ArrayList<>();
    ArrayList<String> nama = new ArrayList<>();
    ArrayList<String> lokasi = new ArrayList<>();
    ArrayList<String> rating = new ArrayList<>();
    ArrayList<String> totalRating = new ArrayList<>();
    ArrayList<String> pengalaman = new ArrayList<>();
    ArrayList<String> profesi = new ArrayList<>();
    ArrayList<String> harga = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_time);
        recyclerView = findViewById(R.id.recyclerViewss);
        toolbar = findViewById(R.id.toolbar16);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void initRecyclerView() {
        adapter = new RecyclerViewAdapter("buytime",this,image,nama,lokasi,totalRating,pengalaman,profesi,harga,rating);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    void initData() {

        for (int i=0;i<10;i++) {
            image.add("https://upload.wikimedia.org/wikipedia/commons/f/f5/Steve_Jobs_Headshot_2010-CROP2.jpg");
            nama.add("Steve Job");
            lokasi.add("Jakarta");
            totalRating.add("(199)");
            pengalaman.add("Pengalaman: > 5 tahun");
            profesi.add("Programmer");
            harga.add("Rp. 1.000.000,-/jam");
            rating.add("5");
        }

        initRecyclerView();
    }
}
