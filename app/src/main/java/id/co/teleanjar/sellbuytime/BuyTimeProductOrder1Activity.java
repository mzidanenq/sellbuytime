package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class BuyTimeProductOrder1Activity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    RecyclerViewAdapter mRecyclerViewAdapter;
    ArrayList<String> kerjaan = new ArrayList<>();
    ArrayList<String> harga = new ArrayList<>();
    ArrayList<String> notes = new ArrayList<>();
    ArrayList<String> jumlah = new ArrayList<>();
    Button submit;
    String Harga;
    TextView mHarga;
    Toolbar toolbar;
    static BuyTimeProductOrder1Activity fo1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_time_product_order1);
        mRecyclerView = findViewById(R.id.recyclerView);
        toolbar = findViewById(R.id.toolbar14);
        submit = findViewById(R.id.button19);
        mHarga = findViewById(R.id.textView96);
        Harga = "Rp100.000";
        fo1 = BuyTimeProductOrder1Activity.this;
        mHarga.setText(Harga);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BuyTimeProductOrder1Activity.this, BuyTimeProductOrder2Activity.class);
                intent.putExtra("harga",Harga);
                startActivity(intent);
            }
        });


        initData();
    }
    public static BuyTimeProductOrder1Activity getInstance() {
        return fo1;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void initRecyclerView() {
        mRecyclerViewAdapter = new RecyclerViewAdapter("productorder",this,null,kerjaan,harga,notes,jumlah,null,null,null);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

    void initData() {
        for (int i= 0; i<5; i++) {
            kerjaan.add("Service AC");
            harga.add("Rp100.000");
            notes.add("test");
            jumlah.add("1");
        }
        initRecyclerView();
    }
}
