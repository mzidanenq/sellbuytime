package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class HistoryActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerViewAdapter recyclerViewAdapter;
    RecyclerView recyclerView;

    ArrayList<String> imagedata = new ArrayList<>();
    ArrayList<String> data1 = new ArrayList<>();
    ArrayList<String> data2 = new ArrayList<>();
    ArrayList<String> data3 = new ArrayList<>();
    ArrayList<String> data4 = new ArrayList<>();
    ArrayList<String> data5 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        toolbar = findViewById(R.id.toolbar23);
        recyclerView = findViewById(R.id.recyclerviews);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        initData();
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("historyorder", this, imagedata,data1,data2,data3,data4,data5,null,null);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    void initData() {

        for (int i=0; i < 10; i++) {
            imagedata.add("https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_960_720.png");
            data1.add("Tukijo");
            data2.add("Teknisi AC");
            data3.add("Rp175.788 / 2 paket");
            data4.add("26 agustus 2018");
            data5.add("Jl Lamandau laminto");
        }

        initRecyclerView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
