package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class ResetPasswordActivity extends AppCompatActivity {
    Toolbar toolbar;
    EditText email;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        toolbar = findViewById(R.id.toolbar8);
        email = findViewById(R.id.editText7);
        intent = null;

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String valEmail = getIntent().getExtras().getString("email",null);
        email.setText(valEmail);
    }

    public void clickDaftarsekarng(View view) {
        intent = new Intent(this, RegisterActivity.class);
        if (intent!=null) {
            startActivity(intent);
            finish();
        }
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
