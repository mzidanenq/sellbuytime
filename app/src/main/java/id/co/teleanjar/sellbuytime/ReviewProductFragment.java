package id.co.teleanjar.sellbuytime;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.zip.Inflater;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class ReviewProductFragment extends Fragment {
    ArrayList<String> mImageLink = new ArrayList<>();
    ArrayList<String> mName = new ArrayList<>();
    ArrayList<String> mLocation = new ArrayList<>();
    ArrayList<String> mExpert = new ArrayList<>();
    ArrayList<String> mAmountOrder = new ArrayList<>();
    ArrayList<String> mPrice = new ArrayList<>();
    ArrayList<String> mRatingAmount = new ArrayList<>();
    ArrayList<String> mRatingBar = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_review_product, container, false);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }
    void initRecyclerView() {
        recyclerView = getView().findViewById(R.id.recyclerViews);
        adapter = new RecyclerViewAdapter("review",getActivity(), mImageLink, mName,mLocation,mExpert,mAmountOrder,mPrice,mRatingAmount,mRatingBar);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    void initData() {

        for (int i=0;  i<5; i++) {
            mImageLink.add("https://fortunedotcom.files.wordpress.com/2014/05/168808285.jpg");
            mName.add("Larry Page");
            mLocation.add("Silicon Valley");
            mExpert.add("Programmer & CEO");
            mAmountOrder.add("1 Unit Google");
            mPrice.add("Rp. 3.205.200,-");
            mRatingAmount.add("(192)");
            mRatingBar.add("2");

            initRecyclerView();
        }

    }

}