package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.Objects;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class ProfileExperienceActivity extends AppCompatActivity {
    RecyclerViewAdapter recyclerViewAdapter;
    RecyclerView recyclerView;
    ArrayList<String> title = new ArrayList<>();
    ArrayList<String> tanggal = new ArrayList<>();
    ArrayList<String> desc = new ArrayList<>();
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_experience);
        recyclerView = findViewById(R.id.recyclerViewss);
        toolbar = findViewById(R.id.toolbar6);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        initData();
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("experience",this, null, title, tanggal,desc,null,null,null,null);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    void initData() {
        for (int i=0; i<15; i++){
            title.add("Teknisi AC merek Daikin");
            tanggal.add("21/21/2121-22/22/2222");
            desc.add("Bekerja sebagai teknisi ac Daikin. menangani berbagai kerusakan baik AC rumah, kantor, maupun pabrik.");
        }
        initRecyclerView();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
