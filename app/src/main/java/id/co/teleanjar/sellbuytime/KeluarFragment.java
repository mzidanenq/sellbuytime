package id.co.teleanjar.sellbuytime;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;


public class KeluarFragment extends Fragment {
    View view;
    ArrayList<String> mNama = new ArrayList<>();
    ArrayList<String> mImageLink = new ArrayList<>();
    ArrayList<String> mWaktu = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;

    public KeluarFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_keluar, container,false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initImageBitmaps();
    }

    void initRecyclerView() {
        recyclerView = getView().findViewById(R.id.recyclerViews);
        adapter = new RecyclerViewAdapter("listitem",getActivity(),mImageLink, mNama,mWaktu,null,null,null,null,null);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    void initImageBitmaps() {

        mImageLink.add("https://i.pinimg.com/564x/37/73/52/377352c489acb98643d50de2762de7b5.jpg");
        mNama.add("Joe Scarborough");
        mWaktu.add("12/12/2012. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/48/f7/83/48f7834dd8ad72c9162919fbbf913c75.jpg");
        mNama.add("John Doe");
        mWaktu.add("12/12/2012. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/83/64/af/8364afc2d9cc7cde7010c8004a1f8820.jpg");
        mNama.add("Très chic");
        mWaktu.add("12/12/2013. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/5f/c3/e9/5fc3e9170d96991b51c34bdf99800061.jpg");
        mNama.add("edgy");
        mWaktu.add("12/12/2012. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/7a/6b/d4/7a6bd4465d9c28213edb2bcae4a549bc.jpg");
        mNama.add("Ms. Monroe");
        mWaktu.add("12/12/2014. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/cd/32/71/cd3271c3c4e82d84c25039c94dfa8004.jpg");
        mNama.add("Moschino lorem ipsum dolor si amet");
        mWaktu.add("12/12/2012. 12:12");

        mImageLink.add("https://i.pinimg.com/564x/a4/8c/e6/a48ce618b2226637eb9f8c4d7d4db9de.jpg");
        mNama.add("Twiggy");
        mWaktu.add("12/12/2012. 12:12");

        initRecyclerView();
    }
}
