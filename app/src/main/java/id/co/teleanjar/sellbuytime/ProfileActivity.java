package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;
import jp.wasabeef.blurry.Blurry;

public class ProfileActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    ArrayList<String> mNama = new ArrayList<>();
    ArrayList<String> mNote = new ArrayList<>();
    ArrayList<String> mHarga = new ArrayList<>();
    FloatingActionButton mFab, mFabMessage, mFabReview, mFabRegister;
    ConstraintLayout clMessage,clRegister,clReview;
    Animation mShowButton,mHideButton,mLayoutShow,mLayoutHide;
    Integer resource;
    Toolbar toolbar;
    Button btnExperience;
    ImageView backgrounds;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        backgrounds = findViewById(R.id.frameLayout);
        btnExperience = findViewById(R.id.button5);
        btnExperience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, ProfileExperienceActivity.class);
                startActivity(intent);
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        );

        //get image from bitmap folder
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.best_linkedin_profile_examples);

        //blur image
        Blurry.with(this)
                .radius(50)
                .sampling(2)
                .async()
                .color(Color.argb(160,0,0,0))
                .from(image)
                .into(backgrounds);

//        Blurry.with(this)
//                .radius(10)
//                .sampling(8)
//                .color(Color.argb(66, 255, 255, 0))
//                .async()
//                .animate(500)
//                .onto((ViewGroup) findViewById(R.id.filterblack));


        recyclerView = findViewById(R.id.recyclerViewss);
        recyclerView.setNestedScrollingEnabled(false);
        mFab = findViewById(R.id.floatingActionButton_plus);
        mFabMessage = findViewById(R.id.floatingActionButton_message);
        mFabRegister = findViewById(R.id.floatingActionButton_register);
        mFabReview = findViewById(R.id.floatingActionButton_review);
        clMessage = findViewById(R.id.constraintLayout_message);
        clRegister = findViewById(R.id.ConstraintLayout_register);
        clReview = findViewById(R.id.constraintLayout_review);
        mShowButton = AnimationUtils.loadAnimation(ProfileActivity.this, R.anim.show_button);
        mHideButton = AnimationUtils.loadAnimation(ProfileActivity.this, R.anim.hide_button);
        mLayoutHide = AnimationUtils.loadAnimation(ProfileActivity.this,R.anim.hide_layout);
        mLayoutShow = AnimationUtils.loadAnimation(ProfileActivity.this,R.anim.show_layout);
        initData();
        Fab();
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void initRecyclerView() {
        adapter = new RecyclerViewAdapter("listjeniskeahlian",this,null, mNama,mNote, mHarga,null,null,null,null);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    void initData() {

        mNama.add("Tukang AC");
        mNote.add("Note: Harus dingin");
        mHarga.add("Rp. 105.000,-");

        mNama.add("Tukang Mobil");
        mNote.add("Note: Harus bener");
        mHarga.add("Rp. 105.000,-");

        mNama.add("Tukang Hp");
        mNote.add("Note: Harus ada sinyal");
        mHarga.add("Rp. 90.000,-");

        mNama.add("Tukang Kursi");
        mNote.add("Note: Harus bisa di dudukin");
        mHarga.add("Rp. 108.000,-");

        mNama.add("Tukang Meja");
        mNote.add("Note: Harus bisa di lipat");
        mHarga.add("Rp. 105.000,-");
        initRecyclerView();
    }
    void Fab() {
        clMessage.setVisibility(View.GONE);
        clRegister.setVisibility(View.GONE);
        clReview.setVisibility(View.GONE);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clMessage.getVisibility() == View.VISIBLE && clRegister.getVisibility() == View.VISIBLE && clReview.getVisibility() == View.VISIBLE) {
                    clMessage.setVisibility(View.GONE);
                    clRegister.setVisibility(View.GONE);
                    clReview.setVisibility(View.GONE);
                    mFab.startAnimation(mHideButton);
                    clMessage.startAnimation(mLayoutHide);
                    clReview.startAnimation(mLayoutHide);
                    clRegister.startAnimation(mLayoutHide);
                } else {
                    clMessage.setVisibility(View.VISIBLE);
                    clRegister.setVisibility(View.VISIBLE);
                    clReview.setVisibility(View.VISIBLE);
                    clMessage.startAnimation(mLayoutShow);
                    clReview.startAnimation(mLayoutShow);
                    clRegister.startAnimation(mLayoutShow);
                    mFab.startAnimation(mShowButton);
                }
            }
        });

        mFabMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, ProfileChatListActivity.class);
                startActivity(intent);
            }
        });
        mFabReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, ProfileReviewActivity.class);
                startActivity(intent);
            }
        });
        mFabRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, RegSellerActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void clickBackProfile(View view) {
        finish();
    }

}
