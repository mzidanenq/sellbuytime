package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class PostOrderActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView iklan;
    Button button;
    RecyclerViewAdapter recyclerViewAdapter;
    ArrayList<String> mImage = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_order);
        toolbar = findViewById(R.id.toolbar12);
        iklan = findViewById(R.id.recyclerView);
        button = findViewById(R.id.button15);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PostOrderActivity.this, PostOrderReviewActivity.class);
                startActivity(intent);
            }
        });
        initData();
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("iklan", this,mImage,null,null,null,null,null,null,null);
        iklan.setLayoutManager(new GridLayoutManager(this,2,GridLayoutManager.HORIZONTAL,false));
        iklan.setAdapter(recyclerViewAdapter);
    }

    void initData() {
        for (int i=0; i<24; i++) {
            mImage.add("https://www.nouveau.co.uk/wp/wp-content/uploads/2017/08/we-are-hiring-image-e1502452302975.jpg");
        }
        initRecyclerView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
