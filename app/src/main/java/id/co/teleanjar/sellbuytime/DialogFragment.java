package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DialogFragment extends android.support.v4.app.DialogFragment {
    TextView mText;
    View view;
    Button btnOk;

    public DialogFragment() {
    }

    public static DialogFragment newInstance(String paragraf, String additional) {
        DialogFragment fragment = new DialogFragment();
        Bundle args = new Bundle();
        args.putString("paragraf", paragraf);
        args.putString("jumlahlayout",additional);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_dialog, container, false);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        setCancelable(false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mText = view.findViewById(R.id.textView86);
        btnOk = view.findViewById(R.id.button14);
        String paragraf = getArguments().getString("paragraf","Terima Kasih");
        final String mjl = getArguments().getString("jumlahlayout","satu");
        mText.setText(paragraf);
//        mText.requestFocus();
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.bg_dialogfragment);
//        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (mjl) {
                    case "productorderreviewactivity":
                        BuyTimeProductOrder2Activity.getInstance().finish();
                        BuyTimeProductOrder1Activity.getInstance().finish();
                        getActivity().finish();
                        break;
                    case "dashboard":
                        Intent intent = new Intent(getContext(), DashboardActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        break;
                    case "satu":
                        getActivity().finish();
                        break;
                        default:
                            getActivity().finish();
                            break;
                }

                dismiss();
            }
        });
    }
}
