package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class ProgressSellerOrderOfferingActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerViewAdapter recyclerViewAdapter;
    RecyclerView recyclerview;
    ArrayList<String> image = new ArrayList<>();
    ArrayList<String> data1 = new ArrayList<>();
    ArrayList<String> data2 = new ArrayList<>();
    ArrayList<String> data3 = new ArrayList<>();
    ArrayList<String> data4 = new ArrayList<>();
    ArrayList<String> data5 = new ArrayList<>();
    ArrayList<String> data6 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_seller_order_offering);

        toolbar = findViewById(R.id.toolbar26);
        recyclerview = findViewById(R.id.recyclerview);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        for (int a=0;a<10;a++) {
            image.add("https://auth.cengr.commonspotcloud.com/engineering/customcf/iws_ai_faculty_display/ai_images/caa238-profile.jpg");
            data1.add("Ferguso");
            data2.add("Latino");
            data3.add("Bersihkan AC Window");
            data4.add("2 unit");
            data5.add("Rp.150.000");
            data6.add("21/08/2018 10.10 AM");
        }
        recyclerViewAdapter = new RecyclerViewAdapter("orderoffering",this,image,data1,data2,data3,data4,data5,data6,null);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setAdapter(recyclerViewAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}