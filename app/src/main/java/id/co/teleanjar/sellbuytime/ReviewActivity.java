package id.co.teleanjar.sellbuytime;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;

import java.util.Objects;

import id.co.teleanjar.sellbuytime.lib.SectionsPagerAdapter;


public class ReviewActivity extends AppCompatActivity {

    TabLayout tabLayout;
    AppBarLayout appBarLayout;
    ViewPager viewPager;
    Toolbar toolBar;
    SectionsPagerAdapter adapter;

    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        tabLayout = findViewById(R.id.tabs);
        appBarLayout = findViewById(R.id.appbar);
        viewPager = findViewById(R.id.viewPager);
        toolBar = findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        adapter.AddFragment(new ReviewPostOrderFragment(), "Post Order");
        adapter.AddFragment(new ReviewProductFragment(), "Product");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);


    }
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
