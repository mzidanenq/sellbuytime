package id.co.teleanjar.sellbuytime;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class ProgressSellerOrderBiddingActivity extends AppCompatActivity {
    Toolbar toolbar;
    Button button;
    EditText mAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_seller_order_bidding);
        toolbar = findViewById(R.id.toolbar10);
        button = findViewById(R.id.button13);
        mAmount = findViewById(R.id.amount);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Wait a Seconds...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.getProgress();
                progressDialog.show();
                mAmount.clearFocus();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        showDialogFragment();
                    }
                },5000);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void showDialogFragment() {
        FragmentManager fm = getSupportFragmentManager();
        String message = "Terimakasih anda telah memberikan konfirmasi pekerjaan yang diterima, dan segera diinformasikan kepada customer untuk melakukan pembayaran silahkan cek progress order di menu \"progress\" dan email anda\n";
        DialogFragment df = DialogFragment.newInstance(message,"satu");
        df.show(fm,"fragment_dialog");
    }
}
