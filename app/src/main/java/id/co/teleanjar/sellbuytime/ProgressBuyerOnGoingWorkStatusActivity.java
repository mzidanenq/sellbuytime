package id.co.teleanjar.sellbuytime;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class ProgressBuyerOnGoingWorkStatusActivity extends AppCompatActivity {
    Toolbar toolbar;

    Button btnSeller, btnBuyer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_buyer_on_going_work_status);
        toolbar = findViewById(R.id.toolbar25);
        setSupportActionBar(toolbar);
        btnSeller = findViewById(R.id.button22);
        btnBuyer = findViewById(R.id.button23);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String status = getIntent().getStringExtra("status");

        switch (status) {
            case "finished":
                btnSeller.setBackgroundResource(R.drawable.btn_login_filled);
                btnSeller.setText("Finished");
                btnBuyer.setBackgroundResource(R.drawable.btn_orange_filled);
                btnBuyer.setText("Submit");
                btnBuyer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ProgressBuyerOnGoingWorkStatusActivity.this, ReviewOrderRemarkActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                break;
            case "ongoing":
                btnSeller.setBackgroundResource(R.drawable.btn_orange_filled);
                btnSeller.setText("On Going");
                btnBuyer.setBackgroundResource(R.drawable.btn_disable);
                btnBuyer.setClickable(false);
                btnBuyer.setText("Submit");
                break;
            case "owforseller":
                btnSeller.setBackgroundResource(R.drawable.btn_login_filled);
                btnSeller.setText("Submit");

                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Wait a Seconds...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                btnSeller.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressDialog.getProgress();
                        progressDialog.show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                showDialogFragment();
                            }
                        },5000);
                    }
                });
                btnBuyer.setBackgroundResource(R.drawable.btn_disable);
                btnBuyer.setText("Submit");
                break;
        }
    }


    void showDialogFragment() {
        FragmentManager fm = getSupportFragmentManager();
        String message = "Terima kasih anda menyelesaikan order pekerjaan. status \"Finished\" telah diinformasikan kepada buyer, dan menunggu approval atas pekerjaan anda. \n yakinkan buyer telah merespond status anda tersebut dan memberikan penilaian dan review kepada anda\n";
        DialogFragment df = DialogFragment.newInstance(message,"satu");
        df.show(fm,"fragment_dialog");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
