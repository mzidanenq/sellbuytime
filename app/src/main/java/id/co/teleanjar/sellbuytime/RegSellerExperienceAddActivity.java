package id.co.teleanjar.sellbuytime;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.support.v7.widget.Toolbar;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class RegSellerExperienceAddActivity extends AppCompatActivity {

    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormat;
    TextInputEditText mToDate,mFromDate,mHeadline,mSummary;
    Calendar calendar;
    Button btnSave,btnAbort;
    Intent intent;
    Integer id;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_seller_experience_add);

        btnSave = findViewById(R.id.btnAdd);
        mHeadline = findViewById(R.id.headline);
        mFromDate = findViewById(R.id.fromDate);
        mToDate = findViewById(R.id.toDate);
        mSummary = findViewById(R.id.summary);
        toolbar = findViewById(R.id.toolbar7);
        btnAbort = findViewById(R.id.btnAbort);

        setSupportActionBar(toolbar);

        dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ROOT);

        intent = getIntent();
        String Headline = intent.getStringExtra("headline");
        String FromDate = intent.getStringExtra("fromDate");
        String ToDate = intent.getStringExtra("toDate");
        String Summary = intent.getStringExtra("summary");
        id = intent.getIntExtra("id",-1);

        if (Headline != null || FromDate != null || ToDate != null || Summary != null) {
            mHeadline.setText(Headline);
            mFromDate.setText(FromDate);
            mToDate.setText(ToDate);
            mSummary.setText(Summary);
            if (id>-1) {
                btnSave.setText("Update");
            }
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (id>-1) {
                    intent = new Intent();
                    saveData();
                    setResult(RESULT_OK, intent);
                    finish();
                }
                else {
                    intent = new Intent();
                    saveData();
                    setResult(RESULT_FIRST_USER, intent);
                    finish();
                }
            }
        });

        btnAbort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        mFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog("from");
            }
        });

        mToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog("to");
            }
        });
    }

    void saveData() {
        intent.putExtra("id", id);
        intent.putExtra("headline", mHeadline.getText().toString());
        intent.putExtra("fromDate", mFromDate.getText().toString());
        intent.putExtra("toDate", mToDate.getText().toString());
        intent.putExtra("summary", mSummary.getText().toString());
    }


    void showDateDialog(final String type){
        calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                switch (type) {
                    case "from":
                        mFromDate.setText(dateFormat.format(newDate.getTime()));
                        break;
                    case "to":
                        mToDate.setText(dateFormat.format(newDate.getTime()));
                        break;
                }
            }
        },calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @Override
    public void onBackPressed() {
        intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}
