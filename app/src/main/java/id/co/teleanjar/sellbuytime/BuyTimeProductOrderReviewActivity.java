package id.co.teleanjar.sellbuytime;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class BuyTimeProductOrderReviewActivity extends AppCompatActivity {
    RecyclerViewAdapter recyclerViewAdapter;
    RecyclerView recyclerView;
    Button change,order, payment;
    TextView harga;
    ArrayList<String> data = new ArrayList<>();
    String Sharga;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_time_product_order_review);
        recyclerView = findViewById(R.id.recyclerviews);
        toolbar = findViewById(R.id.toolbar13);
        change = findViewById(R.id.button18);
        order = findViewById(R.id.button17);
        harga = findViewById(R.id.textView90);
        payment = findViewById(R.id.btnPayment);
        payment.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Sharga = "Rp100.123";
        harga.setText(Sharga);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Wait a Seconds...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        String tipe = getIntent().getStringExtra("type");

        if (tipe.equals("progressbuyerorder")) {
            change.setVisibility(View.GONE);
            order.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            payment.setVisibility(View.VISIBLE);

            payment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(BuyTimeProductOrderReviewActivity.this, ProgressBuyerOrderPaymentActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BuyTimeProductOrder2Activity.getInstance().finish();
                finish();
            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.getProgress();
                progressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        showDialogFragment("Terima kasih Anda telah memberikan order pekerjaan kepada seller kami \n silahkan menunggu notifikasi order anda telah diterima oleh seller, melalui menu \"progress\" dan email anda \n Segera lakukan pembayaran atas order anda, kemudia seller kami menindaklanjuti order pekerjaan");
                    }
                },5000);
            }
        });

        initData();
    }


    void showDialogFragment(String message) {
        FragmentManager fm = getSupportFragmentManager();
        DialogFragment df = DialogFragment.newInstance(message,"satu");
        df.show(fm,"fragment_dialog");
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("infotransfer",this,null,data,null,null,null,null,null,null);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewAdapter);
    }
    void initData() {
        data.add("* Silahkan Transfer sejumlah Rp100.123 ke salah satu rekening berikut");
        data.add("* Transfer tidak sesuai total sampai 3 digit terakhir akan menghambat masuknya saldo");
        initRecyclerView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
