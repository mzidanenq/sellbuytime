package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.R;
import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class SettingBankAccountActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    RecyclerViewAdapter recyclerViewAdapter;

    ArrayList<String> mImage = new ArrayList<>();
    ArrayList<String> mBankName = new ArrayList<>();
    ArrayList<String> mAccountName = new ArrayList<>();
    ArrayList<String> mAccountNumber = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_bank_account);
        mRecyclerView = findViewById(R.id.recyclerView2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initData();
    }

    public void clickAddBank(View view) {
        Intent intent = new Intent(SettingBankAccountActivity.this, SettingAddBankActivity.class);
        startActivity(intent);
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("bankaccount", this,mImage,mBankName,mAccountName,mAccountNumber,null,null,null,null);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(recyclerViewAdapter);
    }

    void initData() {
        mImage.add("https://cdn.freebiesupply.com/logos/large/2x/bca-bank-central-asia-logo-png-transparent.png");
        mBankName.add("PT. Bank Central Asia");
        mAccountName.add("Udinadin");
        mAccountNumber.add("87654321");

        mImage.add("https://3.bp.blogspot.com/-e1fOq9uUk8M/V15O0WHiIMI/AAAAAAAAAJA/IpxPlLevxLsjisy2I625Yvz-eNzgc6xfgCKgB/s1600/Logo%2BBank%2BBNI%2BPNG.png");
        mBankName.add("PT. Bank Negara Indonesia");
        mAccountName.add("Saprudin");
        mAccountNumber.add("12345678");
        initRecyclerView();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
