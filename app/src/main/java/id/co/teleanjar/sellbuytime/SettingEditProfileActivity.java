package id.co.teleanjar.sellbuytime;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.media.Image;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class SettingEditProfileActivity extends AppCompatActivity {
    ImageView background;
    ConstraintLayout cl_bg;
    Spinner prov,kotaKab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_edit_profile);
//        background = findViewById(R.id.imageView11);
        cl_bg = findViewById(R.id.cl_bg);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        );

        prov = findViewById(R.id.spinner_prov);
        kotaKab = findViewById(R.id.spinner_kotakab);

    }

    public void clickBackEditProfile(View view) {
        finish();
    }
}
