package id.co.teleanjar.sellbuytime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class FeedbackActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    ArrayList<String> mImage = new ArrayList<>();
    ArrayList<String> mText = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mRecyclerView = findViewById(R.id.recyclerViewss);
        initData();
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("feedback", this,mImage,null,null,null,null,null,null,null);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,3));
        mRecyclerView.setAdapter(recyclerViewAdapter);
    }

    void initData() {
        for (int i=0; i<24; i++) {
            mImage.add("https://www.nouveau.co.uk/wp/wp-content/uploads/2017/08/we-are-hiring-image-e1502452302975.jpg");
        }
        initRecyclerView();
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
