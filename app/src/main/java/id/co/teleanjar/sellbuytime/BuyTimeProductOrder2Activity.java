package id.co.teleanjar.sellbuytime;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BuyTimeProductOrder2Activity extends AppCompatActivity {
    Toolbar toolbar;
    Button submit;
    EditText mDate,mClock,mBudget;

    static BuyTimeProductOrder2Activity fo2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_time_product_order2);

        fo2 = BuyTimeProductOrder2Activity.this;
        toolbar = findViewById(R.id.toolbar15);
        submit = findViewById(R.id.btnSubmit);
        mDate = findViewById(R.id.editText11);
        mClock = findViewById(R.id.editText12);
        mBudget = findViewById(R.id.editText10);

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        DateFormat dfc = new SimpleDateFormat("h:mm a");
        String clock = dfc.format(Calendar.getInstance().getTime());
        mDate.setText(date);
        mClock.setText(clock);
        mBudget.setText(getIntent().getStringExtra("harga"));

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BuyTimeProductOrder2Activity.this, BuyTimeProductOrderReviewActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    public static BuyTimeProductOrder2Activity getInstance() {
        return fo2;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
