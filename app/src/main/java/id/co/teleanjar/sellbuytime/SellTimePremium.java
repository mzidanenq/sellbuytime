package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class SellTimePremium extends AppCompatActivity {
    TextView judul,body;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_time_premium);
        judul = findViewById(R.id.textView87);
        body = findViewById(R.id.textView88);
        toolbar = findViewById(R.id.toolbar11);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        body.setText(R.string.joblist_body_premium);
        judul.setText("PT Cocacola Amatil Indonesia");

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
