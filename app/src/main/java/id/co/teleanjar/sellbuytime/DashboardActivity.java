package id.co.teleanjar.sellbuytime;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView notif;
    Intent intent;
    Button buytime,regSeller,buttonSellTime,postOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        buytime = findViewById(R.id.btn_buytime);
        regSeller = findViewById(R.id.btn_regseller);
        buttonSellTime = findViewById(R.id.btn_selltime);
        postOrder = findViewById(R.id.btn_post);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        notif = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_progress));

        initializeCountdrawer();

        buttonSellTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(DashboardActivity.this,SellTimeActivity.class);
                startActivity(intent);
            }
        });


        buytime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(DashboardActivity.this,BuyTimeActivity.class);
                startActivity(intent);
            }
        });

        regSeller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(DashboardActivity.this,RegSellerActivity.class);
                startActivity(intent);
            }
        });

        postOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(DashboardActivity.this, PostOrderActivity.class);
                startActivity(intent);
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);

            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
    }


    private void initializeCountdrawer() {
        notif.setGravity(Gravity.CENTER_VERTICAL);
        notif.setTypeface(null, Typeface.BOLD);

        notif.setTextColor(getResources().getColor(R.color.colorAccent));

        String kk = "+99  21";

        SpannableString colorText = new SpannableString(kk);
        colorText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorOrange)), kk.indexOf("+99"), kk.indexOf("+99") + "+99".length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        notif.setText(colorText);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        intent = null;
        if (id == R.id.nav_message) {
            intent = new Intent(DashboardActivity.this, ChatActivity.class);
        } else if (id == R.id.nav_profile) {
            intent = new Intent(DashboardActivity.this, ProfileActivity.class);
        } else if (id == R.id.nav_progress) {
            intent = new Intent(DashboardActivity.this, ProgressActivity.class);
        } else if (id == R.id.nav_review) {
            intent = new Intent(DashboardActivity.this, ReviewActivity.class);
        } else if (id == R.id.nav_feedback) {
            intent = new Intent(DashboardActivity.this, FeedbackActivity.class);
        } else if (id == R.id.nav_setting) {
            intent = new Intent(DashboardActivity.this, SettingActivity.class);
        } else if (id == R.id.nav_logout) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Apakah anda ingin keluar ?").setIcon(R.drawable.anon).setPositiveButton("Keluar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    intent = new Intent(DashboardActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }).setNegativeButton("Batal", null);
            alertDialogBuilder.show();
        }
        if (intent != null){
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
