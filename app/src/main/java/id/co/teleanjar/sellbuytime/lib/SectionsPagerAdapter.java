package id.co.teleanjar.sellbuytime.lib;

import android.media.Image;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    List<Fragment> list = new ArrayList<>();
    List<String> title = new ArrayList<>();

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }


    @Override
    public int getCount() {
        return  title.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return  title.get(position);
    }

    public void AddFragment(Fragment fragment, String Title) {
        list.add(fragment);
        title.add(Title);
    }
}
