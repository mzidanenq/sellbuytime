package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;
import id.co.teleanjar.sellbuytime.model.OnGoingModel;

public class ProgressBuyerOnGoingWorkActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    ArrayList<OnGoingModel> ongoingmodel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_buyer_on_going_work);
        toolbar = findViewById(R.id.toolbar24);
        recyclerView = findViewById(R.id.recyclerview);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initData();
    }

    void initData() {
        String tipe = getIntent().getExtras().getString("jenis");
        if (tipe.equals("owforseller")) {
            ongoingmodel.add(new OnGoingModel(0,"https://apollo-singapore.akamaized.net/v1/files/i3mz1wzq65te3-ID/image;s=966x691;olx-st/_1_.jpg","Tukijo","Teknisi AC", "Rp.150000/2 paket","21 agustus 2017","Jl nangka 1000"));
            ongoingmodel.add(new OnGoingModel(0,"https://apollo-singapore.akamaized.net/v1/files/i3mz1wzq65te3-ID/image;s=966x691;olx-st/_1_.jpg","Tukijo","Teknisi AC", "Rp.150000/2 paket","21 agustus 2017","Jl nangka 1000"));
        } else {
            ongoingmodel.add(new OnGoingModel(1,"https://apollo-singapore.akamaized.net/v1/files/i3mz1wzq65te3-ID/image;s=966x691;olx-st/_1_.jpg","Tukijo","Teknisi AC", "Rp.150000/2 paket","21 agustus 2017","Jl nangka 1000"));
            ongoingmodel.add(new OnGoingModel(0,"https://apollo-singapore.akamaized.net/v1/files/i3mz1wzq65te3-ID/image;s=966x691;olx-st/_1_.jpg","Tukijo","Teknisi AC", "Rp.150000/2 paket","21 agustus 2017","Jl nangka 1000"));
        }
        initRecyclerViewAdapter();
    }

    void initRecyclerViewAdapter() {
        String tipe = getIntent().getExtras().getString("jenis");
        if (tipe.equals("owforseller")) {
            recyclerViewAdapter = new RecyclerViewAdapter("owforseller",ongoingmodel,this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(recyclerViewAdapter);
        } else {
            recyclerViewAdapter = new RecyclerViewAdapter("ongoingwork",ongoingmodel,this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(recyclerViewAdapter);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
