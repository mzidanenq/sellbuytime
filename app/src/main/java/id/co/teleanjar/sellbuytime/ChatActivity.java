package id.co.teleanjar.sellbuytime;

import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;

import id.co.teleanjar.sellbuytime.lib.SectionsPagerAdapter;

public class ChatActivity extends AppCompatActivity {

    TabLayout tabLayout;
    AppBarLayout appBarLayout;
    ViewPager viewPager;
    Toolbar toolBar;
    SectionsPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        tabLayout = findViewById(R.id.tabs);
        appBarLayout = findViewById(R.id.appbar);
        viewPager = findViewById(R.id.viewPager);
        toolBar = findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        onSupportNavigateUp();
//        toolBar.setNavigationIcon(R.drawable.ic_chat);
        adapter = new SectionsPagerAdapter(getSupportFragmentManager());

        adapter.AddFragment(new InboxFragment(), "Inbox");
        adapter.AddFragment(new KeluarFragment(), "Keluar");
        adapter.AddFragment(new ArsipFragment(), "Arsip");
        adapter.AddFragment(new SampahFragment(), "Sampah");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
