package id.co.teleanjar.sellbuytime;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kofigyan.stateprogressbar.StateProgressBar;

public class RegSellerSkillFragment extends Fragment {
    Button btnSave;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_reg_seller_skill,container,false);

        btnSave = view.findViewById(R.id.buttonSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((RegSellerActivity)getActivity()).viewPager.setCurrentItem(2,true);
                ((RegSellerActivity)getActivity()).stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
            }
        });


        return view;
    }
}
