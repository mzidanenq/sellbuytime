package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import java.util.Calendar;

public class RegSellerProductAddActivity extends AppCompatActivity {
    TextInputEditText mKetProduct,mHarga,mNote;
    Spinner mUnitPrice;
    Button btnSave,btnAbort;
    Intent intent;
    Integer id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_seller_product_add);

        btnSave = findViewById(R.id.button11);
        btnAbort = findViewById(R.id.button12);
        mKetProduct = findViewById(R.id.ketProduct);
        mHarga = findViewById(R.id.ketHarga);
        mNote = findViewById(R.id.note);
        mUnitPrice = findViewById(R.id.unitPrice2);

        intent = getIntent();
        String ketProduct = intent.getStringExtra("ketproduct");
        String harga = intent.getStringExtra("harga");
        String unitprice = intent.getStringExtra("unitprice");
        String note = intent.getStringExtra("note");
        id = intent.getIntExtra("id",-1);

        if (ketProduct != null || harga != null || unitprice != null || note != null) {
            mKetProduct.setText(ketProduct);
            mHarga.setText(harga);
//            mUnitPrice.setSelection(0);
            mNote.setText(note);
            if (id>-1) {
                btnSave.setText("Update");
            }
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (id>-1) {
                    intent = new Intent();
                    saveData();
                    setResult(RESULT_OK,intent);
                    finish();
                } else {
                    intent = new Intent();
                    saveData();
                    setResult(RESULT_FIRST_USER,intent);
                    finish();
                }
            }
        });
        btnAbort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent();
                setResult(RESULT_CANCELED,intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        intent = new Intent();
        setResult(RESULT_CANCELED,intent);
        finish();
    }

    void saveData() {
        intent.putExtra("id", id);
        intent.putExtra("ketproduct", mKetProduct.getText().toString());
        intent.putExtra("harga", mHarga.getText().toString());
        intent.putExtra("unitprice", mUnitPrice.getSelectedItem().toString());
        intent.putExtra("note",mNote.getText().toString());
    }
}
