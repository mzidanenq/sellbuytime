package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class ProgresBuyerOrderActivity extends AppCompatActivity {

    ArrayList<String> mImageLink = new ArrayList<>();
    ArrayList<String> mName = new ArrayList<>();
    ArrayList<String> mLocation = new ArrayList<>();
    ArrayList<String> mExpert = new ArrayList<>();
    ArrayList<String> mAmountOrder = new ArrayList<>();
    ArrayList<String> mPrice = new ArrayList<>();
    ArrayList<String> mRatingAmount = new ArrayList<>();
    ArrayList<String> mRatingBar = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progres_buyer_order);
        recyclerView = findViewById(R.id.recyclerViewss);
        toolbar = findViewById(R.id.toolbar21);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void initRecyclerView() {
        adapter = new RecyclerViewAdapter("progressbuyerorder",this, mImageLink, mName,mLocation,mExpert,mAmountOrder,mPrice,mRatingAmount,mRatingBar);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    void initData() {

        for (int i=0;  i<5; i++) {
            mImageLink.add("https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/07/10/104577141-GettyImages-688402786.1910x1000.jpg");
            mName.add("Mark Zuckerberg");
            mLocation.add("Silicon Valley");
            mExpert.add("Programmer");
            mAmountOrder.add("2 unit Aplikasi");
            mPrice.add("Rp. 2.000.000,-");
            mRatingAmount.add("(9233)");
            mRatingBar.add("4.5");

            mImageLink.add("https://fortunedotcom.files.wordpress.com/2014/05/168808285.jpg");
            mName.add("Larry Page");
            mLocation.add("Silicon Valley");
            mExpert.add("Programmer & CEO");
            mAmountOrder.add("1 Unit Google");
            mPrice.add("Rp. 3.205.200,-");
            mRatingAmount.add("(192)");
            mRatingBar.add("2");

            initRecyclerView();
        }

    }
}
