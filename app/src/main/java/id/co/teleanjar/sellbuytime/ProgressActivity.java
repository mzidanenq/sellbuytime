package id.co.teleanjar.sellbuytime;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class ProgressActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    ArrayList<String> mImage = new ArrayList<>();

    ImageView pOrder,ogWork,history;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);
        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.recyclerView3);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        pOrder = findViewById(R.id.imageView8);
        ogWork = findViewById(R.id.imageView9);
        history = findViewById(R.id.imageView6);


        pOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProgressActivity.this, ProgresBuyerOrderActivity.class);
                startActivity(intent);
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProgressActivity.this, HistoryActivity.class);
                startActivity(intent);
            }
        });
        ogWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProgressActivity.this, ProgressBuyerOnGoingWorkActivity.class);
                startActivity(intent);
            }
        });
        initData();
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("iklan", this,mImage,null,null,null,null,null,null,null);
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    void initData() {
        for (int i=0; i<24; i++) {
            mImage.add("https://www.nouveau.co.uk/wp/wp-content/uploads/2017/08/we-are-hiring-image-e1502452302975.jpg");
        }
        initRecyclerView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void clickPOforSeller(View view) {
        Intent intent = new Intent(ProgressActivity.this, ProgressSellerOrderOfferingActivity.class);
        startActivity(intent);
    }

    public void clickOngoingWorkforSeller(View view) {
        Intent intent = new Intent(ProgressActivity.this, ProgressBuyerOnGoingWorkActivity.class);
        intent.putExtra("jenis", "owforseller");
        startActivity(intent);
    }
}
