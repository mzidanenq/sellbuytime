package id.co.teleanjar.sellbuytime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import id.co.teleanjar.sellbuytime.lib.RecyclerViewAdapter;

public class ProgressBuyerOrderPaymentActivity extends AppCompatActivity {
    RecyclerView info,bank;
    RecyclerViewAdapter recyclerViewAdapter, recyclerViewAdapter2;
    ArrayList<String> data = new ArrayList<>();
    Toolbar toolbar;

    ArrayList<String> LogoBank = new ArrayList<>();
    ArrayList<String> NoRekBank = new ArrayList<>();
    ArrayList<String> AnBank = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_buyer_order_payment);
        toolbar = findViewById(R.id.toolbar22);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        info = findViewById(R.id.recyclerView5);
        bank = findViewById(R.id.recyclerview21);


        initData();
        initDataBank();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void initRecyclerView() {
        recyclerViewAdapter = new RecyclerViewAdapter("infotransfer",this,null,data,null,null,null,null,null,null);
        info.setLayoutManager(new LinearLayoutManager(this));
        info.setAdapter(recyclerViewAdapter);
    }
    void initData() {
        data.add("* Silahkan Transfer sejumlah Rp100.123 ke salah satu rekening berikut");
        data.add("* Transfer tidak sesuai total sampai 3 digit terakhir akan menghambat masuknya saldo");
        initRecyclerView();
    }

    void initRecyclerViewBank() {
        recyclerViewAdapter2 = new RecyclerViewAdapter("banklist",this,LogoBank,NoRekBank,AnBank,null,null,null,null,null);
        bank.setLayoutManager(new LinearLayoutManager(this));
        bank.setAdapter(recyclerViewAdapter2);
    }

    void initDataBank() {
        LogoBank.add("https://3.bp.blogspot.com/-e1fOq9uUk8M/V15O0WHiIMI/AAAAAAAAAJA/IpxPlLevxLsjisy2I625Yvz-eNzgc6xfgCKgB/s1600/Logo%2BBank%2BBNI%2BPNG.png");
        NoRekBank.add("123-456-78");
        AnBank.add("a/n PT SellBuyTime cab kayun");

        LogoBank.add("https://png2.kisspng.com/sh/ac7e21081c37cba2581fd8e23b7fd29c/L0KzQYm3V8E0N6Z0gpH0aYP2gLBuTfJidpwyhdN3ZHn1eX7pgf5sNZ9qf9N7YT3sfrX2jvV0cZIyetN3az3mdbB7kvFtNZJ4RdVqcnWwfLFuj702amUATahtZkO2Q7fsUb42O2Y9UaMDOEG4Q4K8U8MxOmQ3SKsELoDxd1==/kisspng-bank-mandiri-bank-negara-indonesia-bank-central-as-care-logo-5b4956df333fe1.5358918815315330232099.png");
        NoRekBank.add("987-654-321");
        AnBank.add("a/n PT SellBuyTime cab kayun");

        initRecyclerViewBank();
    }
}
